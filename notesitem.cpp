﻿#include "notesitem.h"

#include <QMenu>

NotesItem::NotesItem(PlatanusPluginInterface* plugin)
  : PlatanusItem(QObject::tr("Notes"))
  , plugin_(plugin)
{
}

PlatanusPluginInterface* NotesItem::plugin() const
{
  return plugin_;
}

void NotesItem::buildContextMenu(QMenu& menu) const
{
  // 自身へのポインタをアクションに登録してメニューに追加する。
  QVariant p = QVariant::fromValue((void*)this);
  QStringList names({"addDir"});
  foreach (QString name, names)
  {
    if (QAction* action = plugin_->getAction(name))
    {
      action->setData(p);
      menu.addAction(action);
    }
  }
}
