﻿#ifndef DIRECTORYITEM_H
#define DIRECTORYITEM_H

#include <platanus/platanusitem.h>

/**
 * @brief DirectoryItemクラス
 */
class DirectoryItem
    : public PlatanusItem
{
public:
  /**
   * @brief コンストラクタ
   * @param text タイトルテキスト
   */
  explicit DirectoryItem(const QString& text);

  /**
   * @brief デストラクタ
   */
  virtual ~DirectoryItem();

  /**
   * @brief コンテキストメニューの構築
   * @param menu メニュー
   */
  void buildContextMenu(QMenu& menu) const override;
};

#endif // DIRECTORYITEM_H
