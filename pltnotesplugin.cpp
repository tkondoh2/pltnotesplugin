﻿#include "pltnotesplugin.h"
#include <ntlx/core/main.h>
#include <ntlx/core/env.h>
#include "notesitem.h"
#include "directoryitem.h"

#include <QFileDialog>
#include <QtDebug>

PltNotesPlugin::PltNotesPlugin(QObject* parent)
  : QObject(parent)
  , notesItem_(Q_NULLPTR)
{
  QAction* action;

  // ディレクトリアイテム追加アクションを追加。
  action = insertAction("addDir", tr("Add Directory"));
  connect(action, &QAction::triggered
          , this, &PltNotesPlugin::addDirectory
          );
}

PltNotesPlugin::~PltNotesPlugin()
{
}

PlatanusItem* PltNotesPlugin::rootItem()
{
  // ルートアイテムがなければNotesItemを生成
  if (!notesItem_)
    notesItem_ = new NotesItem(this);

  return notesItem_;
}

void PltNotesPlugin::addDirectory()
{
  // Notesデータディレクトリをデフォルトにしてダイアログを表示する。
  QString dataDir = ntlx::env::dataDirectory().toQString();
  QString newDir = QFileDialog::getExistingDirectory(
        parentWindow()
        , tr("Local Directory")
        , dataDir
        );
  if (newDir.isEmpty())
    return;

  if (QAction* action = qobject_cast<QAction*>(sender()))
  {
    // 呼び出し元のアクションからアイテムを特定する。
    QVariant v = action->data();
    if (PlatanusItem* item = reinterpret_cast<PlatanusItem*>(v.value<void*>()))
    {
      // アイテムの下にディレクトリアイテムを追加する。
      item->appendRow(new DirectoryItem(newDir));
    }
  }
}
