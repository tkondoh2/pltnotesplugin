﻿#ifndef PLTNOTESPLUGIN_H
#define PLTNOTESPLUGIN_H

#include "pltnotesplugin_global.h"
#include <platanus/platanusplugininterface.h>
#include "notesitem.h"

/**
 * @brief PltNotesPlugin(Platanus用Notesプラグイン)
 */
class PLTNOTESPLUGINSHARED_EXPORT PltNotesPlugin
    : public QObject
    , public PlatanusPluginInterface
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID PLATANUS_PLUGIN FILE "pltnotesplugin.json")
  Q_INTERFACES(PlatanusPluginInterface)

public:
  /**
   * @brief デフォルトコンストラクタ
   * @param parent 親オブジェクト
   */
  explicit PltNotesPlugin(QObject* parent = Q_NULLPTR);

  /**
   * @brief デストラクタ
   */
  ~PltNotesPlugin();

  /**
   * @brief ルートアイテムを返す。
   * @return ルートアイテム(NotesItem)
   */
  PlatanusItem* rootItem() override;

public slots:

  /**
   * @brief ディレクトリアイテムの追加処理
   */
  void addDirectory();

private:
  NotesItem* notesItem_;
};

#endif // PLTNOTESPLUGIN_H
