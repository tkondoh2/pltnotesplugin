﻿#ifndef NOTESITEM_H
#define NOTESITEM_H

#include <platanus/platanusitem.h>

/**
 * @brief NotesItemクラス
 */
class NotesItem
    : public PlatanusItem
{
public:
  /**
   * @brief コンストラクタ
   * @param plugin プラグイン
   */
  explicit NotesItem(PlatanusPluginInterface* plugin);

  /**
   * @brief プラグインを返す。
   * @return プラグインへのポインタ
   */
  PlatanusPluginInterface* plugin() const override;

  /**
   * @brief コンテキストメニューを構築する。
   * @param menu メニュー
   */
  void buildContextMenu(QMenu& menu) const override;

private:
  PlatanusPluginInterface* plugin_;
};

#endif // NOTESITEM_H
