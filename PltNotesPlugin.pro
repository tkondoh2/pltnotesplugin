#-------------------------------------------------
#
# Project created by QtCreator 2018-02-22T22:27:13
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = PltNotesPlugin
TEMPLATE = lib
CONFIG += plugin

DEFINES += PLTNOTESPLUGIN_LIBRARY

SOURCES += pltnotesplugin.cpp \
    notesitem.cpp \
    directoryitem.cpp

HEADERS += pltnotesplugin.h\
        pltnotesplugin_global.h \
    notesitem.h \
    directoryitem.h

#
# Notes API用マクロ定義
#
win32 {
    DEFINES += W W32 NT
    contains(QMAKE_TARGET.arch, x86_64) {
        DEFINES += W64 ND64 _AMD64_
    }
}
else:macx {
    # MACのDWORDをunsigned int(4バイト)にする
    # DEFINES += MAC
    DEFINES += MAC LONGIS64BIT
}
else:unix {
    DEFINES += UNIX LINUX W32
    QMAKE_CXXFLAGS += -std=c++0x
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    .gitignore \
    pltnotesplugin.json

LIBS += -lnotes -lntlx_core

# プラグインが必要とするライブラリは、Platanusの実行環境でパスを通す必要がある。
# Macの場合は、DYLD_LIBRARY_PATHに以下のパスを通して確認した。
# /Applications/IBM Notes.app/Contents/MacOS
# /Users/TakahideKondoh/Projects/build-ntlx-Desktop_Qt_5_9_3_clang_64bit-Debug/core
# なお、Macは:(コロン)区切り、スペース文字はクォート文字で囲う必要はない。
